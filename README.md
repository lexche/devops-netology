# devops-netology
First string

# скрытые папки terraform
**/.terraform/*

# файлы с расширением .tfstate
*.tfstate
# файлы в чьих названиях присутствует .tfstate.
*.tfstate.*

# Лог файл с соответствующим названием
crash.log

# файлы с соответствующим расширением
*.tfvars

# Соответствующие файлы 
override.tf
override.tf.json
#и группы файлов в чьих названиях встречается
*_override.tf
*_override.tf.json

# Файлы
.terraformrc
terraform.rc
